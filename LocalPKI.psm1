#Requires -RunAsAdministrator

function New-LocalCA()
{
    Param (
        [String]$DisplayName = "My Local CA",
        [int]$Length = 10
    )

    $Now = Get-date
    $Expires = $Now.AddYears($Length)

    $CACertificate = New-SelfSignedCertificate `
                            -Subject $DisplayName `
                            -NotAfter $Expires `
                            -KeyUsage None `
                            -KeyLength 4096 `
                            -CertStoreLocation Cert:\LocalMachine\My `
                            -SuppressOid "2.5.29.37" `
                            -KeyExportPolicy NonExportable

    # Put the certificate into the TrustedRoot store on this machine
    Move-Item -Path $CACertificate.PSPath -Destination Cert:\LocalMachine\Root -PassThru
}

function Get-LocalCA()
{
    Param (
        [String]$Subject
    )

    $AllCAsWithPrivateKey = Get-ChildItem Cert:\LocalMachine\Root | Where-Object {$_.HasPrivateKey} 

    if ($Subject)
    {
        $AllCAsWithPrivateKey | Where-Object {$_.Subject -like "CN=$Subject*"} | Write-Output
    }
    else
    {
        $AllCAsWithPrivateKey | Write-Output
    }
}

function New-LocalSignedCertificate()
{
    Param (
        [Alias("Subject")]
        [String]$DisplayName,

        [Alias("SAN")]
        [Alias("SubjectAlternativeName")]
        [String[]]$DnsName,

        # Certificate validity range from 1 day to 10 years
        [ValidateRange(1,3650)] 
        [int]$ValidForDays = 90,

        [Parameter(ValueFromPipeline)]
        [System.Security.Cryptography.X509Certificates.X509Certificate2]$Signer
    )

    $Now = Get-date
    $Expires = $Now.AddDays($ValidForDays)

    # Move the signing certificate into the personal store
    $SignerInPersonalStore = Move-Item -Path $Signer.PSPath -Destination Cert:\LocalMachine\My -PassThru
    
    # Create a new certificate, signed by the signer
    $CreatedCertificate = New-SelfSignedCertificate `
                            -Subject $DisplayName `
                            -Signer $SignerInPersonalStore `
                            -DnsName $DnsName `
                            -TextExtension "2.5.29.37={text}1.3.6.1.5.5.7.3.1" `
                            -NotAfter $Expires `
                            -CertStoreLocation Cert:\LocalMachine\My `
                            -KeyExportPolicy Exportable
    
    # Move the signer back to the trusted store
    Move-item $SignerInPersonalStore.PSPath -Destination $Signer.PSParentPath

    # output the new certificate
    $CreatedCertificate | Write-Output
}