
This is a PowerShell module which will create a local PKI

The purpose is to create certificates for systems (e.g. IT labs etc) which are all signed by a common root which is trusted by the local end-user device

Functions include:-
- New-LocalCA - a function to create a certificate authority
- Get-LocalCA - gets any certificates in the TrustedRoot store which have an associated private key
- New-LocalSignedCertificate - Creates a certificate signed by a